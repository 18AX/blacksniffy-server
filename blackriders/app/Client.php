<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'owner_id',
        'name',
        'computer_name',
        'operating_system',
        'mac',
        'last_ip',
        'hwid',
        'country',
        'current_interface'
    ];

    protected $casts = [
        'owner_id' => 'integer'
    ];

    private static $auth_clients = [];

    public static function addAuthClients($id, \Ratchet\ConnectionInterface $conn) {
        self::$auth_clients[$id] = $conn;
    }

    public static function removeAuthClients($id) {
        unset(self::$auth_clients[$id]);
    }

    public static function getSocketConnection($id) {
        return self::$auth_clients[$id];
    }

    public static function isConnected($id) {
        return array_key_exists($id, self::$auth_clients);
    }

    public static function createIfNotExist($owner_id, $hwid) {
        $client = self::where('hwid', $hwid);

        if ($client->count() == 0) {
            self::create([
                'owner_id' => $owner_id,
                'hwid' => $hwid
            ]);

            return true;
        }
        return false;
    }

    public static function getClientByHwid($hwid) {
        $client = self::where('hwid', $hwid)->first();

        return $client;
    }
}
