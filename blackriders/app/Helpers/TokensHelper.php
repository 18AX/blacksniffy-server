<?php

if (!function_exists('generate_token')) {
    function generate_token($user_id) {
        $tokens = \App\Token::where('user_id', $user_id);

        $str = rand();
        $result = sha1($str);

        if ($tokens->count() == 0) {
            \App\Token::create([
                'user_id' => $user_id,
                'token' => $result
            ]);
        } else {
            $token = $tokens->first();
            $token->token = $result;
            $token->save();
        }

        return $result;
    }
}

if (!function_exists('check_token')) {
    function check_token($user_id, $token) {
        $tok = \App\Token::where('user_id', $user_id)->first();

        return $tok && $tok->token == $token;
    }
}

if (!function_exists('check_hwid')) {
    function check_hwid($client_id, $hwid) {
        if (!isset($client_id) or !isset($hwid)) {
            echo "return false";
            return false;
        }

        echo "call";

        $client = \App\Client::find($client_id);

        return $client && $client->hwid == $hwid;
    }
}

?>
