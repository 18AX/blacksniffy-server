<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{

    protected $fillable = ['client_id', 'url'];

    protected $casts = [
        'client_id' => 'integer',
    ];

}
