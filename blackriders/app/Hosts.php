<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hosts extends Model
{
    protected $fillable = ['scan_id', 'ip_address', 'mac_address'];

    protected $casts = ['client_id' => 'integer'];
}
