<?php

namespace App\Http\Controllers;

use App\Client;
use App\NetworkInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function clients() {
        $clients = Client::where('owner_id', Auth::user()->id);

        return view("dashboard/clients/clients", compact('clients'));
    }

    public function clientSettings($client_id) {
        $client = Client::find($client_id);

        if (!$client) {
            return abort(404);
        }

        $networkInterfaces = NetworkInterface::where('client_id', $client_id)->get();
        $token = generate_token(Auth::user()->id);

        return view('dashboard/clients/client_settings')
            ->with('client', $client)
            ->with('networkInterfaces', $networkInterfaces)
            ->with('token', $token);
    }

    public function saveOptions($client_id, Request $request){

        $client = Client::find($client_id);

        if (!$client) {
            return abort(404);
        }

        $name = $request->input('name');
        $interface = $request->input('interface');

        if (!isset($name) or !isset($interface)) {
            return redirect('/dashboard/');
        }

        $client->name = $name;
        $client->save();

        // NetworkInterface::where('name', $interface)->first()->id;
        $client->current_interface = $interface;
        $client->save();

        return redirect('/dashboard/clients/settings/'.$client->id);

    }

    public function removeClient(Request $request) {
        $client_id = $request->input('id');

        if (!isset($client_id)) {
            return redirect('/dashboard/');
        }

        Client::find($client_id)->delete();

        return redirect('/dashboard/clients/');
    }
}
