<?php

namespace App\Http\Controllers;

use App\Hosts;
use App\Scans;
use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $clients = Client::all();
        return view('dashboard/index');
    }

    public function help() {
        return view('dashboard/help');
    }

    public function settings() {
        return view('dashboard/user/settings');
    }
}
