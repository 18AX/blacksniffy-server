<?php

namespace App\Http\Controllers;

use App\Client;
use App\History;
use App\NetworkInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MitmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function selectClient() {
        $clients = Client::all();

        if ($clients->count() == 1) {
            return redirect('/dashboard/mitm/client/'.$clients[0]->id);
        }

        return view('dashboard/mitm/mitm', compact('clients'));
    }

    public function showMitmWithClient($client_id) {
        $client = Client::find($client_id);

        if (!$client) {
            return abort(404);
        }

        $token = generate_token(Auth::user()->id);

        $histories = History::where('client_id', $client_id);

        $interface = NetworkInterface::find($client->current_interface);

        $view = view('dashboard/mitm/client')->with('client', $client)->with('token', $token)->with('histories', $histories);

        if ($interface)
            $view->with("interface", $interface);

        return $view;

    }

    public function postPimtWithClient(Request $request, $client_id) {
        $client = Client::find($client_id);

        if (!$client) {
            return abort(404);
        }

        $token = generate_token(Auth::user()->id);

        $histories = History::where('client_id', $client_id);


        if (!$request->has('ip', 'mac')) {
            return abort(404);
        }

        $ip = $request->ip;
        $mac = $request->mac;


        $interface = NetworkInterface::find($client->current_interface);

        $view = view('dashboard/mitm/client')
            ->with('client', $client)
            ->with('token', $token)
            ->with('histories', $histories)
            ->with('ip', $ip)
            ->with('mac', $mac);

        if ($interface)
            $view->with("interface", $interface);

        return $view;

    }
}
