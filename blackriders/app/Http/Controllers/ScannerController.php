<?php

namespace App\Http\Controllers;

use App\Client;
use App\Hosts;
use App\NetworkInterface;
use App\Scans;
use Illuminate\Support\Facades\Auth;

class ScannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function selectClient() {
        $clients = Client::all();

        if ($clients->count() == 1) {
            return redirect('/dashboard/network-scanner/client/'.$clients[0]->id);
        }

        return view('dashboard/networkscanner/network_scanner', compact("clients"));
    }

    public function showWithClient($client_id) {
        $client = Client::find($client_id);

        if (!$client) {
            return abort(404);
        }

        $isConnected = Client::isConnected($client->id);

        $token = generate_token(Auth::user()->id);

        $last_scan = Scans::where('client_id', $client_id)->first();

        $interface = NetworkInterface::find($client->current_interface);

        $view = view('dashboard/networkscanner/client')->with([
            'client' => $client,
            'isConnected' => $isConnected,
            'token' => $token
        ]);

        if ($last_scan) {
            $hosts = Hosts::where('scan_id', $last_scan->id)->get();
            $view->with([
                'last_scan' => $last_scan,
                'hosts' => $hosts
            ]);
        }

        if ($interface) {
            $view->with('interface', $interface);
        }

        return $view;
    }
}
