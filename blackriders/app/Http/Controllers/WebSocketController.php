<?php

namespace App\Http\Controllers;

use App\Client;
use App\History;
use App\Hosts;
use App\NetworkInterface;
use App\User;
use App\Scans;
use Ratchet\MessageComponentInterface;

class WebSocketController extends Controller implements MessageComponentInterface
{
    private $connections = [];

    function onOpen(\Ratchet\ConnectionInterface $conn) {
        $discoId = $conn->resourceId;
        $this->connections[$discoId] = compact('conn') + ['user_type' => null, 'id' => null];

        echo "[*] Client $discoId connected\n";
    }

    function onClose(\Ratchet\ConnectionInterface $conn) {
        $discoId = $conn->resourceId;

        if ($this->connections[$discoId]['user_type'] == "user") {
            $id = $this->connections[$discoId]['id'];

            User::removeAuthUsers($id);

            echo "[*] User $id disconnected\n";
        }

        if ($this->connections[$discoId]['user_type'] == "client") {
            $id = $this->connections[$discoId]['id'];

            Client::removeAuthClients($id);

            echo "[*] Client $id disconnected\n";
        }

        unset($this->connections[$discoId]);
    }

    function onError(\Ratchet\ConnectionInterface $conn, \Exception $e) {
        $discoId = $conn->resourceId;

        unset($this->connections[$discoId]);

        $conn->close();

        echo "[-] error with $discoId disconnecting....\n$e\n";
    }

    function onMessage(\Ratchet\ConnectionInterface $conn, $msg) {
        $data = json_decode($msg, true);

        if (!isset($data['user_type'])) {
            $this->send_error($conn, "User type not found");
            return;
        }

        if (!isset($data['command'])) {
            $this->send_error($conn, "Command not found");
        }
        $user_type = $data['user_type'];
        $command = $data['command'];

        echo "[*] user type : $user_type command : $command\n";

        if ($user_type == "user") {
            $this->proceed_user($conn, $data);
        } else if ($user_type == "client") {
            $this->proceed_client($conn, $data);
        } else {
            $this->send_error($conn, "Unknown user type !");
        }
    }

    private function proceed_user(\Ratchet\ConnectionInterface $conn, $json) {
        $discoId = $conn->resourceId;
        $id = $json['id'];
        $token = $json['token'];

        if (!User::isConnected($this->connections[$discoId]['id'])) {

            if ($json['command'] == "register") {

                if (check_token($id, $token)) {
                    $this->connections[$discoId]['user_type'] = 'user';
                    $this->connections[$discoId]['id'] = $id;

                    User::addAuthUsers($id, $conn);

                    $this->send_success($conn, "You are connected !");
                } else {
                    $this->send_error($conn, "Invalid token !");
                }
            } else {
                $this->send_error($conn, "You are not connected");
            }
        } else {

            if (check_token($id, $token)) {
                $this->proceed_user_command($id, $conn, $json);
            } else {
                $this->send_error($conn, "Invalid token !");
            }
        }
    }

    private function proceed_user_command($id, $conn, $json) {
        $command = $json['command'];
        echo "New command from a register user : $command\n";

        unset($json['token']);

        switch ($command) {
            case 'client-connected':
                $client_id = $json['client_id'];

                $data = array(
                    'client_id' => 'client_id',
                    'command' => 'response-client-connected',
                    'status' => Client::isConnected($client_id)
                );

                $conn->send(json_encode($data));
                break;
            case 'block':
            case 'getCurrentInterface':
            case 'askForData':
            case 'start-scanner':
            case "stop-mitm":
            case "start-mitm":
                $client_id = $json['client_id'];

                if (Client::isConnected($client_id)) {
                    Client::getSocketConnection($client_id)->send(json_encode($json));
                } else {
                    $this->send_error($conn, "Client not connected");
                }
                break;


            default:
                $this->send_error($conn, "Invalid command !");
                break;
        }
    }

    private function proceed_client(\Ratchet\ConnectionInterface $conn, $json) {
        $discoId = $conn->resourceId;

        if (!isset($json['hwid'])) {
            $this->send_error($conn, "Hwid is require");
        }

        $hwid = $json['hwid'];

        if (Client::createIfNotExist($json['owner_id'], $hwid)) {
            $data = array(
                'command' => 'askForData'
            );

            echo "[*] asking for data\n";

            $conn->send(json_encode($data));
            return;
        }

        $client = Client::getClientByHwid($hwid);

        if (!Client::isConnected($client->id)) {
            if ($json['command'] == "register") {

                $this->connections[$discoId]['user_type'] = 'client';
                $this->connections[$discoId]['id'] = $client->id;

                Client::addAuthClients($client->id, $conn);

                $client->last_ip = $conn->remoteAddress;
                $client->save();

                $this->send_success($conn, "You are connected");

            } else {
                $this->send_error($conn, "You are not connected");
            }
        } else {

            if (check_hwid($client->id, $hwid))
                $this->proceed_client_command($client->id, $conn, $json);
            else {
                $this->send_error($conn, "$client->id Invalid HWID $hwid !");
            }
        }
    }

    private function proceed_client_command($id, $conn, $json) {
        $command = $json['command'];
        echo "New command from a register client: $command\n";

        switch ($command) {
            case 'test':
                echo "test\n";
                break;
            case 'response-scanner':
                $user_id = $json['owner_id'];

                $scan = Scans::where('client_id', $id)->first();

                if (!$scan) {
                    $scan = Scans::create([
                        'client_id' => $id,
                        'low_address' => $json['low_address'],
                        'max_address' => $json['max_address'],
                        'tested_devices' => $json['tested_devices']
                    ]);
                }
                Hosts::where('scan_id', $scan->id)->delete();

                foreach ($json['hosts'] as $hosts) {
                    Hosts::create([
                        'scan_id' => $scan->id,
                        'mac_address' => $hosts['Key'],
                        'ip_address' => $hosts['Value'],
                    ]);
                }

                if (User::isConnected($user_id)) {
                    User::getSocketConnection($user_id)->send(json_encode($json));
                } else {
                    $this->send_error($conn, 'User not connected');
                }

                break;
            case 'history':
                $user_id = $json['owner_id'];
                $client = Client::find($id);
                History::create([
                    'client_id' => $client->id,
                    'url' => $json['url'],
                ]);


                if (User::isConnected($user_id)) {
                    User::getSocketConnection($user_id)->send(json_encode($json));
                } else {
                    $this->send_error($conn, 'User not connected');
                }

                break;
            case 'response-askForData':
                $client = Client::find($id);
                $client->owner_id = $json['owner_id'];
                $client->computer_name = $json['computer_name'];
                $client->operating_system = $json['operating_system'];
                $client->country = $json['country'];

                NetworkInterface::where('client_id', $client->id)->delete();

                foreach ($json['networkInterfaces'] as $interface) {
                    NetworkInterface::create([
                        'client_id' => $client->id,
                        'name' => $interface['name'],
                        'description' => $interface['description'],
                        'mac_address' => $interface['mac_address']
                    ]);
                }

                $client->save();
                break;
            case 'website-parameters':
            case 'logs-mitm':
            case 'getCurrentInterface-response':
            case 'error':
                $user_id = $json['owner_id'];

                if (User::isConnected($user_id)) {
                    User::getSocketConnection($user_id)->send(json_encode($json));
                } else {
                    $this->send_error($conn, "User not connected !");
                }
                break;

            default:
                $this->send_error($conn, "Client invalid command : ".$command);
                break;
        }
    }

    private function send_error(\Ratchet\ConnectionInterface $conn, $error) {
        echo "[-] error : $error\n";
        $error = json_encode(array(
            'command' => 'error',
            'message' => $error
        ));

        $conn->send($error);
    }

    private function send_success(\Ratchet\ConnectionInterface $conn, $success) {
        echo "[*] success : $success\n";
        $success = json_encode(array('success' => $success));
        $conn->send($success);
    }


}
