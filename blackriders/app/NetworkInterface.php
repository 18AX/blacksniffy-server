<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NetworkInterface extends Model
{
    protected $fillable = ['id', 'name', 'description', 'client_id', 'mac_address'];

    protected $casts = [
        'id' => 'integer',
        'client_id' => 'integer'
    ];
}
