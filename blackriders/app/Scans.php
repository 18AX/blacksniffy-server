<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scans extends Model
{
    protected $fillable = ['id', 'client_id', 'low_address', 'max_address', 'tested_devices'];

    protected $casts = [
        'client_id' => 'integer',
        'tested_devices' => 'integer'
    ];
}
