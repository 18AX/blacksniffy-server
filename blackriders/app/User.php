<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    private static $auth_users = [];

    public static function addAuthUsers($id, \Ratchet\ConnectionInterface $conn) {
        self::$auth_users[$id] = $conn;
    }

    public static function removeAuthUsers($id) {
        unset(self::$auth_users[$id]);
    }

    public static function getSocketConnection($id) {
        return self::$auth_users[$id];
    }

    public static function isConnected($id) {
        return array_key_exists($id, self::$auth_users);
    }
}
