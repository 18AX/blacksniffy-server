@extends('layouts/default', ['page_name' => 'About us'])

@section('content')
    <div class="container-fluid text-center">


        <div class="page-container">

            <p>
                We are a team of 4 French students at EPITA. We made Blacksniffy because we did not want to do a video game for our school project. <br> We interesting in cyber security and networks. We decided to create this tool to simplify Hacking to everyone, even for those that do not know anything.
            </p>

            <div class="row mt-5">
                <div class="col-md">
                    <img src="img/ugo.jpg" width="150px" height="150px" class="rounded-circle" alt="ugo">
                    <h2 class="mt-4">Ugo Tarot</h2>
                    <h5>Student at EPITA</h5>
                    <h6>App Developer</h6>
                </div>

                <div class="col-md">
                    <img src="img/liann.jpg" width="150px" height="150px" class="rounded-circle" alt="ugo">
                    <h2 class="mt-4">Liann Pelhate</h2>
                    <h5>Student at EPITA</h5>
                    <h6>App developer and communication</h6>
                </div>

                <div class="col-md">
                    <img src="img/antoine.jpg" width="150px" height="150px" class="rounded-circle" alt="Liann">
                    <h2 class="mt-4">Antoine</h2>
                    <h5>Student at EPITA</h5>
                    <h6>App developer</h6>
                </div>

                <div class="col-md">
                    <img src="img/alex.jpg" width="150px" height="150px" class="rounded-circle" alt="alex">
                    <h2>Alex</h2>
                    <h5>Student at EPITA</h5>
                    <h6>App developer and Web developer</h6>
                </div>
            </div>
        </div>
    </div>
@endsection
