@extends('dashboard.layouts.dashboard', ['page_name' => $client->name, 'selected_item' => 2])

@section('content')
    <div class="panel">
        <div class="panel-top">
            General
        </div>
        <div class="panel-content">
            <form class="text-center" method="post" action="">
                @csrf
                <div class="d-flex justify-content-between">

                    <div class="row">

                        <label  class="col-md-2 col-form-label text-md-right" for="name">Name</label>
                        <div class="col-md-3">
                            <input id="name" name="name" value="{{$client->name}}" class="form-control" type="text" style="width: 350px">
                        </div>

                    </div>

                    <div class="row">
                        <label  class="col-md-2 col-form-label text-md-right" for="name">Select interface</label>
                        <div class="col-md-3">

                                <select name="interface" class="form-control" style="width: 350px">
                                    @foreach($networkInterfaces as $net)
                                        <option value="{{$net->id}}">
                                            @if($net->id == $client->current_interface)
                                                {{$net->name}} (current)
                                            @else
                                                {{$net->name}}
                                            @endif

                                        </option>
                                    @endforeach
                                </select>

                        </div>
                    </div>
                </div>

                <button class="btn button-success" type="submit">Save !</button>
            </form>
        </div>
    </div>

@endsection
