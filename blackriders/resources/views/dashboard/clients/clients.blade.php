@extends('dashboard.layouts.dashboard', ['page_name' => 'Clients', 'selected_item' => 2])

@section('content')

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="data_sum">
                    <h3 class="text-md-center">{{ $clients->count() }} clients registered</h3>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="data_sum" style="border-color: #78e08f;">
                    <h3 class="text-md-center">{{ 0 }} clients connected</h3>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="data_sum" style="border-color: #4a69bd;">
                    <h3 class="text-md-center">{{ 0 }} packet intercepted</h3>
                </div>
            </div>
        </div>

        <table class="mt-5">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Computer Name</th>
                    <th>Operating System</th>
                    <th>Mac Address</th>
                    <th>Last IP Address</th>
                    <th>HWID</th>
                    <th>Country</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

                @foreach ($clients->cursor() as $client)
                    <tr>
                        <td>{{$client->name}}</td>

                        <td>{{$client->computer_name}}</td>

                        <td>{{$client->operating_system}}</td>

                        <td>{{$client->mac}}</td>

                        <td>{{$client->last_ip}}</td>

                        <td>{{$client->hwid}}</td>

                        <td>{{$client->country}}</td>

                        <td>
                            <form class="" action="/dashboard/network-scanner/client/{{$client->id}}" method="get">
                                <button class="btn button-success" type="submit">Scanner</button>
                            </form>
                        </td>
                        <td>
                            <form class="" action="/dashboard/clients/remove" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{$client->id}}">
                                <button class="btn button-error" type="submit">Remove</button>
                            </form>
                        </td>

                        <td>
                            <form action="/dashboard/clients/settings/{{$client->id}}" method="get">
                                <input type="submit" class="settings-button" alt="Settings button" value="">
                            </form>
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>

@endsection
