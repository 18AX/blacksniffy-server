@extends('dashboard/layouts/dashboard', ['page_name' => 'Help', 'selected_item' => 0])

@section('content')
    <div class="panel">

        <div class="panel-content">
            <h1 class="mt-3">Join us !</h1>

            <hr>

            <p>You can join our community and our team on Discord.</p>
            <div class="text-center">
                <iframe src="https://discordapp.com/widget?id=543537054307319818&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0"></iframe>
            </div>
        </div>
    </div>
@endsection
