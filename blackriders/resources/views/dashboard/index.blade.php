@extends('dashboard/layouts/dashboard', ['page_name' => 'Home', 'selected_item' => 1])

@section('content')

    <div class="panel">
        <div class="panel-content">
            <h1 class="mt-4">Welcome on Black@Sniffy</h1>
            <hr>
            <p>Black@Sniffy is a useful tool created by the Black corporation. We made an easy-2-use app to monitor your network using the Man In The Middle attack. Never forget that your are fully <b>responsible</b> of what your are doing with this tool. In case of judical case, you data may be shared with the police.</p>
        </div>
    </div>

@endsection
