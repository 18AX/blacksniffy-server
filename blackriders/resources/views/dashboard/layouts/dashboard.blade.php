<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Black@Sniffy - {{ $page_name }}</title>
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

        <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

    </head>
    <body>
        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <div class="border-right" id="sidebar-wrapper">
              <div class="sidebar-heading">Black@Sniffy</div>
              <div class="list-group list-group-flush">

                    <a class="{{$selected_item == 1 ? "selected-btn" : "unselect-btn"}}" href="/dashboard">Home</a>
                    <a class="{{$selected_item == 2 ? "selected-btn" : "unselect-btn"}}" href="/dashboard/clients">Clients</a>
                    <a class="{{$selected_item == 3 ? "selected-btn" : "unselect-btn"}}" href="/dashboard/network-scanner">Network Scanner</a>
                    <a class="{{$selected_item == 4 ? "selected-btn" : "unselect-btn"}}" href="/dashboard/mitm">Man In The Middle</a>
                    <a class="unselect-btn" href="#">Plans</a>
              </div>
            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper" class="main-container">

              <nav id="page-content-top" class="navbar navbar-expand-lg navbar-light border-bottom">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }}
                      </a>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/dashboard/settings">Settings</a>
                        <a class="dropdown-item" href="/dashboard/help">Need help ?</a>
                        <div class="dropdown-divider"></div>
                        <form id="logout-btn" class="dropdown-item" action="{{ route('logout') }}" method="POST">
                            @csrf
                            Log out
                        </form>
                      </div>
                    </li>
                  </ul>
                </div>
              </nav>

              <div class="mt-5 container-fluid ">
                  @yield('content')
              </div>
            </div>
            <!-- /#page-content-wrapper -->

          </div>
          <!-- /#wrapper -->



          <!-- Menu Toggle Script -->
          <script>
            $("#menu-toggle").click(function(e) {
              e.preventDefault();
              $("#wrapper").toggleClass("toggled");
            });

            $("#logout-btn").click(function(e) {
              $("#logout-btn").submit();
            });

          </script>
    </body>
</html>
