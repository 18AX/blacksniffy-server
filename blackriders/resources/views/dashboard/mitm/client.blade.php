@extends('dashboard/layouts/dashboard', ['page_name' => $client->name, 'selected_item' => 4])


@section('content')
    <div class="panel">

        <div class="panel-top">
            Client Information
        </div>

        <div class="panel-content">
            <div class="d-flex justify-content-between">
                <div>
                    <h2>Client Name : {{$client->name}}</h2>
                </div>
                <div id="client-status">

                    <h4 class="disconnected-status">Not connected</h4>

                </div>
            </div>
        </div>
    </div>

    <div class="panel mt-5">
        <div class="panel-top">Man In The Middle</div>
        <div class="panel-content">

            <div class=" justify-content-center text-center mt-3">
                <div class="form-group row">

                    <label class="col-md-2 col-form-label text-md-right" for="victim_address">Victim ip address</label>


                    <div class="col-md-3">
                        <input id="victim_address" value="{{$ip ?? ''}}" class="form-control" type="text" placeholder="ex: 192.168.0.50" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" style="width: 350px">
                    </div>


                    <label class="col-md-2 col-form-label text-md-right" for="victim_mac">Victim mac</label>


                    <div class="col-md-3">
                        <input id="victim_mac" value="{{$mac ?? ''}}" class="form-control" type="text" placeholder="ex: cc:72:30:5d:e8:8c" pattern="^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$" style="width: 350px">
                    </div>

                </div>

                <div class="form-group row mt-3">

                    <label class="col-md-2 col-form-label text-md-right" for="router_address">Routeur ip address</label>


                    <div class="col-md-3">
                        <input id="router_address" class="form-control" type="text" placeholder="ex: 192.168.0.0" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" style="width: 350px">
                    </div>


                    <label class="col-md-2 col-form-label text-md-right" for="router_mac">Router mac</label>


                    <div class="col-md-3">
                        <input id="router_mac" class="form-control" type="text" placeholder="ex: f7:f4:1e:b9:52:4b" pattern="^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$" style="width: 350px">
                    </div>


                </div>


                <div class="mt-5">
                    <button class="btn button-success" onclick="javascript:startMitm(victim_address.value, victim_mac.value, router_address.value, router_mac.value)" style="width: 100px">Start !</button>
                    <button class="btn button-error" onclick="stopMitm()" style="width: 100px">Stop !</button>


                    <div class="form-check">
                        <input type="checkbox" onclick="block(this)" class="form-check-input" id="block_checkbox">
                        <label class="form-check-label" for="block_checkbox">Block connection</label>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md">

            <table class="table">
                <thead>
                    <tr>
                        <th>IP Address</th>
                        <th>Website</th>
                        <th>Date time</th>
                    </tr>
                </thead>

                <tbody id="history">
                    @foreach($histories as $history)
                        <tr>
                            <td>
                                {{$history->ip_address}}
                            </td>
                            <td>
                                {{$history->url}}
                            </td>
                            <td>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="col-md">

            <table class="table">
                <thead>
                    <tr>
                        <th>Website</th>
                        <th>Key</th>
                        <th>Value</th>
                    </tr>
                </thead>

                <tbody id="parameters">

                </tbody>
            </table>
        </div>
    </div>

    <div class="panel mt-5">
        <div class="panel-top">Live logs</div>
        <div class="panel-content">
            <div class="form-group">
                <textarea id="logs" readonly class="form-control rounded-0" ></textarea>
            </div>
        </div>
    </div>

    @include('dashboard/layouts/partials/notification')



    <script type="text/javascript">
        var conn = new WebSocket('ws://127.0.0.1:8090');

        conn.onopen = function (e) {
            console.log("connected");
            conn.send(JSON.stringify({
                user_type: "user",
                id: {{Auth::user()->id}},
                token: "{{$token}}",
                command: "register"
            }));

            conn.send(JSON.stringify({
                user_type: "user",
                id: {{Auth::user()->id}},
                token: "{{$token}}",
                command: "client-connected",
                client_id: {{$client->id}},

            }));
        };

        conn.onmessage = function (e) {
            var json = JSON.parse(e.data);

            console.log(json);

            if (json.command === 'logs-mitm') {


                var count = document.getElementById('logs').value.length;

                if (count > 500) {
                    document.getElementById('logs').innerHTML = "";
                }

                document.getElementById("logs").innerHTML += json.logs + "\n";
            }

            if (json.command === 'website-parameters') {
                document.getElementById("parameters").innerHTML +=
                    "<tr><td>" + json.website + "</td>" + "<td>" + json.key + "</td>" + "<td>" + json.value + "</td></tr>";

                var parameters = json.parameters;

                for (i = 0; parameters.length; i++) {
                    document.getElementById("parameters").innerHTML +=
                        "<tr><td>" + json.website + "</td>" + "<td>" + parameters[i].Key +
                        "</td>" + "<td>" + parameters[i].Value + "</td></tr>";
                }
            }

            if (json.command === 'website-history') {

            }

            if (json.command === 'response-client-connected') {

                if (json.status) {
                    document.getElementById('client-status').innerHTML =
                        "<h4 class=\"connected-status\">Connected</h4>"
                } else {
                    document.getElementById('client-status').innerHTML =
                        "<h4 class=\"disconnected-status\">Not connected</h4>"
                }
            }

            if (json.command === 'error') {
                console.log("error");
                document.getElementById('toast-body').innerHTML = json.message;
                $('.toast').toast('show');
            }
        };

        function startMitm(victim_addr, victim_mac, router_addr, router_mac) {

            conn.send(JSON.stringify({
                user_type: "user",
                id: {{Auth::user()->id}},
                client_id: {{$client->id}},
                token: "{{$token}}",
                command: "start-mitm",
                victim_addr: victim_addr,
                victim_mac: victim_mac,
                router_addr: router_addr,
                router_mac: router_mac,
                device: "{{$interface->name ?? "None"}}"
            }));
        }

        function stopMitm() {
            conn.send(JSON.stringify({
                user_type: "user",
                id: {{Auth::user()->id}},
                client_id: {{$client->id}},
                token: "{{$token}}",
                command: "stop-mitm",
            }));
        }

        function block(check) {
            conn.send(JSON.stringify({
                user_type: "user",
                id: {{Auth::user()->id}},
                client_id: {{$client->id}},
                token: "{{$token}}",
                command: "block",
                status: check.checked,
            }));
        }
    </script>
@endsection
