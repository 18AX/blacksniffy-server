@extends('dashboard.layouts.dashboard', ['page_name' => $client->name, 'selected_item' => 3])

@section('content')

    <div class="panel mt-4">
        <div class="panel-top">
            Client information
        </div>

        <div class="panel-content">
            <div class="d-flex justify-content-between">
                <div>
                    <h2>Client Name : {{$client->name}}</h2>
                </div>
                <div id="client-status">

                    <h4 class="disconnected-status">Not connected</h4>

                </div>
            </div>
        </div>


        <hr />
        <form class=" justify-content-center text-center" action="javascript:startScanner(low_address.value, max_address.value)">
            <div class="row">
                <div class="col-md">
                    <label for="low_address">Low address</label>
                </div>

                <div class="col-md">
                    <input id="low_address" class="form-control" type="text" placeholder="ex: 192.168.0.0" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" style="width: 350px">
                </div>

                <div class="col-md">
                    <label for="max_address">Max address</label>
                </div>

                <div class="col-md">
                    <input id="max_address" class="form-control" type="text" placeholder="ex: 192.168.0.190" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" style="width: 350px">
                </div>

                <div class="col-md">
                    <button class="btn button-success" value="Click" style="width: 150px">Start !</button>
                </div>

            </div>

        </form>
    </div>

    <div class="panel mt-5">

        <div class="panel-top">Localisation</div>
        <div class="panel-content">

            <div id="map">

            </div>
        </div>
    </div>


    <div class="panel mt-5">
        <div class="panel-top">Scan statistics</div>
        <div class="panel-content">
            <h4 class="network-scanner-devices text-center">
                @if (isset($hosts))
                {{$hosts->count()}} devices discovered</h4>
                @else
                    No scan founds
                @endif

            <div class="row">

            </div>

        </div>
    </div>

    <table class="mt-5">
        <thead>
        <tr>
            <th>IP Address</th>
            <th>Mac Address</th>
            <th></th>
        </tr>
        </thead>
        <tbody id="hosts">
        @isset($hosts)
        @foreach($hosts as $scan)
            <tr>
                <td>{{$scan->ip_address}}</td>
                <td>{{$scan->mac_address}}</td>
                <td>
                    <form method="post" action="/dashboard/mitm/client/{{$client->id}}">
                        @csrf
                        <input type="hidden" name="ip" value="{{$scan->ip_address}}">
                        <input type="hidden" name="mac" value="{{$scan->mac_address}}">
                        <button type="submit" class="btn button-success">Launch Mitm</button>
                    </form>
                </td>
            </tr>
        @endforeach
        @endisset
        </tbody>
    </table>

    @include('dashboard/layouts/partials/notification')


    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">
        var conn = new WebSocket('ws://127.0.0.1:8090');

        conn.onopen = function (e) {
            console.log("connected");
            conn.send(JSON.stringify({
                user_type: "user",
                id: {{Auth::user()->id}},
                token: "{{$token}}",
                command: "register"
            }));

            conn.send(JSON.stringify({
                user_type: "user",
                id: {{Auth::user()->id}},
                token: "{{$token}}",
                command: "client-connected",
                client_id: {{$client->id}}

            }));

            initMap();
        };

        conn.onmessage = function (e) {
            var json = JSON.parse(e.data);

            if (json.command === 'response-scanner') {

                var hosts = json.hosts;

                document.getElementById('hosts').innerHTML = "";

                for (i = 0; hosts.length; i++) {
                    document.getElementById('hosts').innerHTML +=
                        "<tr><td>" + hosts[i].Key + "</td>" +
                        "<td>" + hosts[i].Value + "</td>" +
                        "<td>" +
                        "<form method=\"post\" action=\"/dashboard/mitm/client/{{$client->id}}\">\n" +
                        "                        <input type=\"hidden\" name=\"_token\" value=\"{{csrf_token()}}\">\n" +
                        "                        <input type=\"hidden\" name=\"ip\" value=\"" + hosts[i].Value + "\">\n" +
                        "                        <input type=\"hidden\" name=\"mac\" value=\"" + hosts[i].Key + "\">\n" +
                        "                        <button type=\"submit\" class=\"btn button-success\">Launch Mitm</button>\n" +
                        "                    </form>" +
                        "</td></tr>";
                }
            }

            if (json.command === 'response-client-connected') {

                if (json.status) {
                    document.getElementById('client-status').innerHTML =
                        "<h4 class=\"connected-status\">Connected</h4>"
                } else {
                    document.getElementById('client-status').innerHTML =
                        "<h4 class=\"disconnected-status\">Not connected</h4>"
                }
            }

            if (json.command === 'error') {
                console.log("error");
                document.getElementById('toast-body').innerHTML = json.message;
                $('.toast').toast('show');
            }
        };

        function startScanner(low, max) {
            conn.send(JSON.stringify({
                user_type: "user",
                id: {{Auth::user()->id}},
                client_id: {{$client->id}},
                token: "{{$token}}",
                command: "start-scanner",
                low: low,
                max: max,
                device: "{{$interface->name ?? "None"}}"
            }));
        }

        var map;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });

        }

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuPHlitom7A1ABQrO0cnENNZhOZTTj8wo&callback=initMap"
            async defer></script>


@endsection()
