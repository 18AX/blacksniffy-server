@extends('/dashboard/layouts/dashboard', ['page_name' => 'Network Scanner', 'selected_item' => 3])

@section('content')
    <div class="panel">
        <div class="panel-top">What is a network scanner ?</div>
        <div class="panel-content">
            <p>You must select a client to start a scan on his network !</p>
        </div>
    </div>

    <table class="mt-5">
        <thead>
        <tr>
            <th>Name</th>
            <th>Computer Name</th>
            <th>Operating System</th>
            <th>Mac Address</th>
            <th>Last IP Address</th>
            <th>HWID</th>
            <th>Country</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        @foreach ($clients as $client)
            <tr>
                <td>{{$client->name}}</td>

                <td>{{$client->computer_name}}</td>

                <td>{{$client->operating_system}}</td>

                <td>{{$client->mac}}</td>

                <td>{{$client->last_ip}}</td>

                <td>{{$client->hwid}}</td>

                <td>{{$client->country}}</td>

                <td>
                    <form class="" action="/dashboard/network-scanner/client/{{$client->id}}" method="get">
                        <button class="btn button-success" type="submit">Use this one !</button>
                    </form>
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
@endsection
