@extends('layouts/default', ['page_name' => 'Home'])

@section('content')

<div class="container-fluid text-center">
    <div class="page-container justify-content-center">
        <div class="row">
            <div class="col m-4" >
                <img src="/img/research.png" width="200px">

                <h2 class="mt-2">Monitor your network !</h2>
                <p>Find all devices connected on your network and see who should not be there...
                </p>

            </div>

            <div class="col m-4">
                <img src="/img/cloud-storage.png" width="200px">

                <h2 class="mt-2">Monitor your network !</h2>
                <p>BlackSniffy can intercept the communication of any device on your local network. With BlackSniffy, your are able to
                    to see uncrypted communication, website visited, ...
                </p>
            </div>

            <div class="col m-4">
                <img src="/img/no-wifi.png" width="200px">
                <h2 class="mt-2">Block devices !</h2>
                <p>BlackSniffy can block the internet connection of any devices on your local network.
                </p>
            </div>
        </div>
    </div>
</div>
    <div class="easy-container mt-5">

            <div class="d-flex justify-content-between">
                <div class="ml-5 mr-5" style="color: #FFFFFF">
                    <h1 class="mt-5">Easy-2-use !</h1>
                    <p class="para mt-5 ">We try to make hacking easy. All our app is designed to be user friendly to people who don't have knowledge in hacking.</p>
                    <p class="para">Moreover, there is nothing to install, you only need a browser and the BlackBox connected on your router.</p>
                </div>

                <div class="m-3">
                    <img src="/img/app.png" height="400px">
                </div>


            </div>
    </div>
    <div class="container-fluid mt-5" style="background-color: #FFFFFF">
        <div class="page-container">
            <div class="row">
                <div class="col-md-4 text-center">
                    <h1 class="mt-5">Join our team on Discord for further information !</h1>
                </div>

                <div class="col-md-8 text-center">
                    <iframe src="https://discordapp.com/widget?id=543537054307319818&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

@endsection
