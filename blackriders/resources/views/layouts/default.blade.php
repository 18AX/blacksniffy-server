<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Black@Sniffy - {{ $page_name }}</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/default.css') }}" rel="stylesheet">
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
</head>
<body>

    @include('layouts/partials/navbar')
    @include('layouts/partials/banner')
    @yield('content')
    @include('layouts/partials/footer')
</body>
</html>
