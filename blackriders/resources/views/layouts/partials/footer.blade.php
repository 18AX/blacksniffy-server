<footer class="py-4 bg-dark text-white">
    <div class="container text-center">
        <h6>Copyright &copy; Blackriders Corporation</h6>
        <h6>2019 - 2020</h6>
    </div>
</footer>
