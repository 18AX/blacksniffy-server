<nav class="navbar navbar-expand-sm navbar-top navbar-light">
    <a class="navbar-brand" href="/">Black@Sniffy</a>

    <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link" href="/pricing">Pricing</a></li>
        <li class="nav-item"><a class="nav-link" href="/about">About us</a></li>
    </ul>

    <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link" href="/login">Login</a></li>
    </ul>
</nav>
