@extends('layouts/default', ['page_name' => 'Pricing'])

@section('content')
<div class="container">

        <div class="row justify-content-center">

            

                <div class="col-md">

                    <ul class="pricing">

                        <li>
                            <h3 style="color: #d35400">Basic</h3>
                        </li>

                        <li>
                            max 1 client
                        </li>

                        <li>
                            Unlimited capture time
                        </li>

                        <li>
                            Basic support
                        </li>

                        <li>
                            <h3 style="color: #2ecc71">4.99€</h3>
                            <span>per month</span>
                        </li>
                    </ul>
                </div>

            <div class="col-md">

                <ul class="pricing">

                    <li>
                        <h3 style="color: #f1c40f">Gold</h3>
                    </li>

                    <li>
                        max 5 client
                    </li>

                    <li>
                        Unlimited capture time
                    </li>

                    <li>
                        Basic support
                    </li>

                    <li>
                        <h3 style="color: #2ecc71">9.99€</h3>
                        <span>per month</span>
                    </li>
                </ul>
            </div>

            <div class="col-md">

                <ul class="pricing">

                    <li>
                        <h3 style="color: #3498db;">Diamond</h3>
                    </li>

                    <li>
                        Unlimited client
                    </li>

                    <li>
                        Unlimited capture time
                    </li>

                    <li>
                        24/24 support
                    </li>

                    <li>
                        <h3 style="color: #2ecc71">19.99€</h3>
                        <span>per month</span>
                    </li>
                </ul>
            </div>
        </div>
</div>
@endsection
