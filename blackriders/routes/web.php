<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['reset' => false]);

Route::get('/', 'HomeController@index');

Route::get('/about', 'HomeController@about');
Route::get('/pricing', 'HomeController@pricing');

Route::get('/dashboard/', 'DashboardController@index');
Route::get('/dashboard/settings', 'DashboardController@settings');
Route::get('/dashboard/help', 'DashboardController@help');

Route::get('/dashboard/clients', 'ClientController@clients');
Route::post('/dashboard/clients/remove', 'ClientController@removeClient');

Route::get('/dashboard/clients/settings/{n}', 'ClientController@clientSettings');
Route::post('/dashboard/clients/settings/{n}', 'ClientController@saveOptions');

Route::get('/dashboard/network-scanner', 'ScannerController@selectClient');
Route::get('/dashboard/network-scanner/client/{n}', 'ScannerController@showWithClient');

Route::get('/dashboard/mitm', 'MitmController@selectClient');
Route::get('/dashboard/mitm/client/{n}', 'MitmController@showMitmWithClient');
Route::post('/dashboard/mitm/client/{n}', 'MitmController@postPimtWithClient');
